using System;
using System.Collections;
using System.Collections.Generic;
using CrocodileBrackets;
using UnityEngine;

[RequireComponent (typeof (InputHandler))]
public class TPController : MonoBehaviour {
    [Header ("Player")]
    [Header ("=====================")]
    [SerializeField] float playerMoveSpeed;
    [SerializeField] float playerRotationSpeed;
    [SerializeField] Transform rayStartPosition;
    [SerializeField] float raycastDistance;
    [SerializeField] LayerMask groundLayer;

    private Rigidbody playerRigidbody;
    private Vector2 moveAction;
    private Vector3 moveDirection;
    public Camera playerCamera;
    private Animator playerAnimator;
    private RaycastHit slopeHit;
    private bool canControlPlayer = true;
    private bool isDashing;

    private void Awake () {
        playerCamera = Camera.main;
        playerRigidbody = GetComponent<Rigidbody> ();
        playerAnimator = GetComponent<Animator> ();
    }

    #region  Input Methods
    public void OnPlayerMoveInput (Vector2 incomingInput) {
        moveAction = incomingInput;
    }
    #endregion

    private void Update () {
        if (canControlPlayer && !isDashing) {
            OnPlayerRotate ();
        }
    }

    private void FixedUpdate () {
        if (canControlPlayer && !isDashing) {
            OnPlayerMove ();
        }
    }

    private void OnPlayerMove () {
        moveDirection = Utilities.CameraBasedInput (playerCamera, moveAction);

        if (OnSlope ()) {
            Vector3 slopeDirection = Vector3.ProjectOnPlane (moveDirection, slopeHit.normal);
            playerRigidbody.velocity = slopeDirection * playerMoveSpeed * Time.fixedDeltaTime;
        } else if (!OnSlope ())
            playerRigidbody.velocity = new Vector3 (moveDirection.x * playerMoveSpeed * Time.fixedDeltaTime, playerRigidbody.velocity.y, moveDirection.z * playerMoveSpeed * Time.fixedDeltaTime);
    }

    private void OnPlayerRotate () {
        transform.forward = Vector3.Lerp (transform.forward, moveDirection, playerRotationSpeed * Time.deltaTime);
    }

    private bool OnSlope () {
        if (Physics.Raycast (rayStartPosition.position, Vector3.down, out slopeHit, raycastDistance, groundLayer)) {
            if (slopeHit.normal != Vector3.up) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}