using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class InputHandler : MonoBehaviour {
    public static InputHandler Instance;
    [SerializeField] CharacterActions actionMap;
    [SerializeField] UnityEvent<Vector2> OnMoveEvent = new UnityEvent<Vector2> ();
    [SerializeField] UnityEvent<float> OnAimEvent = new UnityEvent<float> ();
    [SerializeField] UnityEvent OnJumpEvent = new UnityEvent ();
    [SerializeField] UnityEvent OnDashEvent = new UnityEvent ();

    private void Awake () {
        Instance = this;
    }

    private void OnEnable () {
        actionMap = new CharacterActions ();
        actionMap.Enable ();
        actionMap.Player.Move.performed += OnPlayerMoveHandler;
        actionMap.Player.Move.canceled += OnPlayerMoveHandler;
        actionMap.Player.Aim.canceled += OnPlayerAimHandler;
        actionMap.Player.Aim.performed += OnPlayerAimHandler;
        actionMap.Player.Jump.performed += OnPlayerJumpHandler;
        actionMap.Player.Dash.performed += OnPlayerDashHandler;
    }

    private void OnDisable () {
        actionMap.Dispose ();
    }

    private void OnPlayerDashHandler (InputAction.CallbackContext obj) {
        OnDashEvent.Invoke ();
    }

    private void OnPlayerJumpHandler (InputAction.CallbackContext obj) {
        OnJumpEvent.Invoke ();
    }

    private void OnPlayerAimHandler (InputAction.CallbackContext obj) {
        OnAimEvent.Invoke (obj.ReadValue<float> ());
    }

    private void OnPlayerMoveHandler (InputAction.CallbackContext obj) {
        OnMoveEvent.Invoke (obj.ReadValue<Vector2> ());
    }
}