using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Sword_controller : MonoBehaviour {

    private Vector3 mOffset;

    private Vector3 mousePos;
    private Vector3 mousePoint;
    private float objectZpos;

    private void OnMouseDown () {
        objectZpos = Camera.main.WorldToScreenPoint (transform.position).z;
        mOffset = gameObject.transform.position - GetMouseWorldPos ();
        Debug.Log ("OnMouseDown");
    }

    private Vector3 GetMouseWorldPos () {
        mousePoint = Input.mousePosition;
        mousePoint.z = objectZpos;
        Debug.Log (mousePoint);
        return Camera.main.ScreenToWorldPoint (mousePoint);
    }

    void OnMouseDrag () {

        transform.position = GetMouseWorldPos () + mOffset;
        Debug.Log ("OnMouseDrag");
    }
}