using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableRainEffect : MonoBehaviour {
    [SerializeField] Material rainEffect;
    [SerializeField] Material screenEffect;

    public void EnableDisableEffect (float incomingValue) {
        rainEffect.SetFloat ("_DistortionAmount", incomingValue);
    }
    public void EnableDisableAbilityEffect (float incomingValue) {
        screenEffect.SetFloat ("_Intensity", incomingValue);
    }
}