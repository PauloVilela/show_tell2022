using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraZones : MonoBehaviour {
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    [SerializeField] bool followPlayer;
    [SerializeField] bool LookPlayer;
    private Transform playerRef;
    private void Start () {
        playerRef = FindObjectOfType<TPController> ().transform;
        virtualCamera.enabled = false;

        if (followPlayer)
            virtualCamera.Follow = playerRef;
        if (LookPlayer)
            virtualCamera.LookAt = playerRef;
    }

    private void OnTriggerEnter (Collider other) {
        if (other.TryGetComponent<TPController> (out TPController player))
            virtualCamera.enabled = true;
    }

    private void OnTriggerExit (Collider other) {
        if (other.TryGetComponent<TPController> (out TPController player))
            virtualCamera.enabled = false;
    }
}