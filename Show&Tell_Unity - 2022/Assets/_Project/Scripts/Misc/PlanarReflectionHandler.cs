using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanarReflectionHandler : MonoBehaviour
{
    public GameObject reflectionPlane;
    public Material reflectionMat;
    private RenderTexture renderTarget;
    Camera reflectionCamera;
    Camera mainCamera;

    private void Start()
    {
        GameObject reflectionGO = new GameObject("ReflectionCamera");
        reflectionCamera = reflectionGO.AddComponent<Camera>();
        reflectionCamera.enabled = false;
        mainCamera = Camera.main;

        renderTarget = new RenderTexture(Screen.width, Screen.height, 24);
    }
    private void Update()
    {
        OnPostRender();
    }

    private void OnPostRender()
    {
        RenderReflection();
    }

    private void RenderReflection()
    {
        reflectionCamera.CopyFrom(mainCamera);

        Vector3 cameraDirectionWorldSpace = mainCamera.transform.forward;
        Vector3 cameraUpWorldSpace = mainCamera.transform.up;
        Vector3 cameraPositionWorldSpace = mainCamera.transform.position;

        Vector3 cameraDirectionPlaneSpace = reflectionPlane.transform.InverseTransformDirection(cameraDirectionWorldSpace);
        Vector3 cameraUpPlaneSpace = reflectionPlane.transform.InverseTransformDirection(cameraUpWorldSpace);
        Vector3 cameraPositionPlaneSpace = reflectionPlane.transform.InverseTransformPoint(cameraPositionWorldSpace);

        cameraDirectionPlaneSpace.y *= -1f;
        cameraUpPlaneSpace.y *= -1f;
        cameraPositionPlaneSpace.y *= -1f;

        cameraDirectionWorldSpace = reflectionPlane.transform.TransformDirection(cameraDirectionPlaneSpace);
        cameraUpWorldSpace = reflectionPlane.transform.TransformDirection(cameraUpPlaneSpace);
        cameraPositionWorldSpace = reflectionPlane.transform.TransformPoint(cameraPositionPlaneSpace);

        reflectionCamera.transform.position = cameraPositionWorldSpace;
        reflectionCamera.transform.LookAt(cameraPositionWorldSpace + cameraDirectionWorldSpace, cameraUpWorldSpace);

        reflectionCamera.targetTexture = renderTarget;
        reflectionCamera.Render();
        reflectionMat.SetTexture("_RenderTexture", renderTarget);
    }
}
