using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueButton : MonoBehaviour {
    public DialogueSO dialogueResponse;
    public TextMeshProUGUI buttonText;

    public void OnResponseClicked () {
        DialogueUI.Instance.currentNpc.currentDialogueNode = dialogueResponse;
        DialogueUI.Instance.currentNpc.OnInteractWithNpc ();
    }
}