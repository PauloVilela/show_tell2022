using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcDialogueController : MonoBehaviour {
    public DialogueSO currentDialogueNode;

    private void Awake () {
        OnInteractWithNpc ();
    }

    public void OnInteractWithNpc () {
        DialogueUI.Instance.UpdateDialogueUI (currentDialogueNode, this);
    }
}