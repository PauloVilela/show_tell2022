using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "DialogueSO", menuName = "Show&Tell_Unity - 2022/DialogueNodes", order = 0)]
public class DialogueSO : ScriptableObject {
    public string NpcName;
    public string NpcDialogueLine;

    [System.Serializable]
    public struct ResponsesPlayer {
        public string response;
        public DialogueSO connectingNode;
    }
    public List<ResponsesPlayer> responsesList = new List<ResponsesPlayer> ();
}