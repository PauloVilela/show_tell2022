using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueUI : Singleton<DialogueUI> {

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;
    public GameObject answerButtonPrefab;
    public RectTransform answerParent;
    public NpcDialogueController currentNpc;

    public void UpdateDialogueUI (DialogueSO incomingNode, NpcDialogueController incomingNpc) {
        nameText.text = incomingNode.NpcName;
        dialogueText.text = incomingNode.NpcDialogueLine;

        for (int i = 0; i < answerParent.childCount; i++) {
            answerParent.GetChild (i).gameObject.SetActive (false);
        }

        for (var i = 0; i < incomingNode.responsesList.Count; i++) {
            Transform currentButton = answerParent.GetChild (i);
            currentButton.GetComponent<DialogueButton> ().buttonText.text = incomingNode.responsesList[i].response;
            currentButton.GetComponent<DialogueButton> ().dialogueResponse = incomingNode.responsesList[i].connectingNode;
            currentButton.gameObject.SetActive (true);
        }
        currentNpc = incomingNpc;
    }
}